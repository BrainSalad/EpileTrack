#!/bin/bash

current=`pwd`

buildDir=build_android
installDir=install_android
mkdir -p $buildDir $installDir

buildDir=$(readlink -m "`realpath $buildDir`")
installDir=$(readlink -m "`realpath $installDir`")

export ANDROID_NDK=/opt/android-ndk-r11c
export ANDROID_SDK_ROOT=/opt/android-sdk-linux

export Qt5_DIR=$HOME/Qt/5.6/android_armv7
export Qt5_CMAKE=$Qt5_DIR/lib/cmake
export QML2_IMPORT_PATH=$installDir/lib/x86_64-linux-gnu/qml/


echo "Qt5_CMAKE: $Qt5_CMAKE"

# =============================================================

cd $buildDir

cmake ../ \
-DCMAKE_INSTALL_PREFIX=$installDir \
-DQt5_DIR=$Qt5_CMAKE \
-DCMAKE_TOOLCHAIN_FILE=/usr/share/ECM/toolchain/Android.cmake \
-DQTANDROID_EXPORTED_TARGET=epiletrack \
-DANDROID_APK_DIR=../src/android_apk && \
\
make && \
make install && \
make create-apk-epiletrack && \
\
echo -e "Install to device command:\n\
/opt/android-sdk-linux/platform-tools/adb install $buildDir/epiletrack_build_apk/bin/QtApp-debug-unaligned.apk"
cd $current
