#!/bin/bash

current=`pwd`

buildDir=build_desktop
installDir=install_desktop
mkdir -p $buildDir $installDir

buildDir=$(readlink -m "`realpath $buildDir`")
installDir=$(readlink -m "`realpath $installDir`")

export Qt5_DIR=$HOME/Qt/5.6/gcc_64
export Qt5_CMAKE=$HOME/$Qt5_DIR/lib/cmake
export QML2_IMPORT_PATH=$installDir/lib/x86_64-linux-gnu/qml/

export LD_LIBRARY_PATH=$Qt5_DIR/lib:$LD_LIBRARY_PATH

# =============================================================

cd $buildDir

cmake ../ \
-DCMAKE_INSTALL_PREFIX=$installDir \
-DQt5_DIR=$Qt5_CMAKE && \
\
make && \
make install && \
echo -e "Running epiletrack:" && \
$installDir/bin/epiletrack

cd $current
